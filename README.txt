--- Instructions to set up faceted search using a GSA  ---
1. Install and configure the google_appliance module.
2. Install and configure the gsa_meta module.
3. Create a faceted search using GSA Faceted Search and Index, using the general
 steps outlined http://envisioninteractive.com/drupal/drupal-7-views-with-faceted-filters-without-apachesolr/
 Use GSA search and index instead of database search.

